import "./App.css";
import TopSection from "@sections/top-section/TopSection";

function App() {
  return <TopSection />;
}

export default App;
