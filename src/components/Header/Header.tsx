import style from "./header.module.scss";
const Header = () => {
  return (
    <div className={style.container}>
      <div className={style.logo}>
        <img src="/top-section/logo.png" alt="logo BRIDA" />
        <div className={style.logo_desc}>
          <h1>BRIDA NTB</h1>
          <p>Badan Riset dan Inovasi Daerah NTB</p>
        </div>
      </div>
      <ul className={style.menu}>
        <li>
          <a href="#lokasi Kegiatan">Lokasi Kegiatan</a>
        </li>
        <li>
          <a href="#lokasi Kegiatan">Lokasi Kegiatan</a>
        </li>
        <li>
          <a href="#lokasi Kegiatan">Lokasi Kegiatan</a>
        </li>
      </ul>
    </div>
  );
};

export default Header;
