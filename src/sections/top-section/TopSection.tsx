import Header from "@components/Header/Header";
import Hero from "@components/Hero/Hero";
import Wrapper from "@layouts/Wrapper";
import style from "./TopSection.module.scss";
const TopSection = () => {
  return (
    <div className={style.container}>
      <Wrapper>
        <Header />
        <Hero />
      </Wrapper>
    </div>
  );
};

export default TopSection;
